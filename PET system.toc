\contentsline {chapter}{\numberline {1}Positron Emission Tomography (PET) system }{2}
\contentsline {section}{\numberline {1.1}Introduction}{2}
\contentsline {section}{\numberline {1.2}The Positron Emission Tomography (PET) system physics }{2}
\contentsline {section}{\numberline {1.3}Photon Detectors for Positron Emission Tomography(PET) system}{2}
\contentsline {subsection}{\numberline {1.3.1}Introduction}{2}
\contentsline {subsection}{\numberline {1.3.2}Interactions of Photons with Matter}{3}
\contentsline {subsubsection}{Photoelectric effect:}{3}
\contentsline {subsubsection}{Compton scattering(non-coherent):}{3}
\contentsline {subsubsection}{Pair production:}{3}
\contentsline {subsubsection}{Rayleigh scattering(coherent):}{3}
\contentsline {subsection}{\numberline {1.3.3}Radioactive Decay}{3}
\contentsline {section}{\numberline {1.4}The PET Scintillation Detector}{3}
\contentsline {chapter}{\numberline {2}The Scintillation material }{4}
\contentsline {section}{\numberline {2.1}scintillation material types}{4}
\contentsline {subsection}{\numberline {2.1.1} Physical characteristic of the inorganic scintillator:}{4}
\contentsline {paragraph}{ Pure inorganic scintillator crystal}{5}
\contentsline {paragraph}{ Doped inorganic scintillator crystal }{5}
\contentsline {paragraph}{ Examples of the inorganic scintillation materials:}{5}
\contentsline {paragraph}{ NaI(Tl)}{5}
\contentsline {paragraph}{$LaBr_3\ (Ce)$}{5}
\contentsline {paragraph}{$ Lu_2SiO_5$(LSO)}{5}
\contentsline {paragraph}{$ PbWO_4 $(PWO) }{5}
\contentsline {paragraph}{ YAP:Ce}{5}
\contentsline {paragraph}{YAG:Ce}{5}
\contentsline {paragraph}{BGO}{5}
\contentsline {paragraph}{CaF:Eu}{6}
\contentsline {paragraph}{CsI:Tl}{6}
\contentsline {paragraph}{LuAG:Ce}{6}
\contentsline {paragraph}{GSO}{6}
\contentsline {paragraph}{CWO}{6}
\contentsline {paragraph}{ MLS}{6}
\contentsline {paragraph}{$ Lu_{0.4}Gd_{1.6}SiO_5$(LGSO)}{6}
\contentsline {section}{\numberline {2.2}Monolithic scintillator}{6}
\contentsline {subsection}{\numberline {2.2.1}The drawbacks of the monolithic scintillator are:}{7}
\contentsline {section}{\numberline {2.3}conclusion}{7}
\contentsline {chapter}{\numberline {3}Positron Emission Tomography (PET) Photodetectors}{8}
\contentsline {section}{\numberline {3.1}Introduction}{8}
\contentsline {section}{\numberline {3.2} Photomultiplier Tube (PMT)}{8}
\contentsline {subsection}{\numberline {3.2.1}Photomultiplier Tube (PMT) operation}{8}
\contentsline {section}{\numberline {3.3} Solid state Semiconductor Photodiode}{9}
\contentsline {subsection}{\numberline {3.3.1}Introduction }{9}
\contentsline {subsection}{\numberline {3.3.2}Avalanche Photodiodes}{9}
\contentsline {paragraph}{ The avalanche photodiode (APD) operation:}{10}
\contentsline {subsection}{\numberline {3.3.3} Geiger-mode Avalanche Photodiodes (G-APD)}{10}
\contentsline {subsection}{\numberline {3.3.4}Silicon Drift Detectors (SDDs) }{10}
\contentsline {subsection}{\numberline {3.3.5} Silicon photomultipliers (SiPMs)}{11}
\contentsline {subsubsection}{Introduction to Silicon photomultipliers (SiPMs) }{11}
\contentsline {subsubsection}{ Silicon Photomultipliers operation}{11}
\contentsline {subparagraph}{ The disadvantages of the Silicon photomultipliers (SiPMs) as compared to photo multiplier tube (PMT) are:}{12}
\contentsline {subsection}{\numberline {3.3.6}The Digital Silicon Photomultipliers (dSiPMs)}{12}
\contentsline {section}{\numberline {3.4}Conclusion}{13}
\contentsline {chapter}{\numberline {4} Bibliography}{14}
\contentsline {chapter}{\numberline {5}Glossary}{16}
